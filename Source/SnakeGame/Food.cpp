// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"
//#include "YourProjectPath/ConstructorHelpers.h"//CHATGPT

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodMesh"));//CHATGPT
	RootComponent = FoodMeshComponent;//CHATGPT

	// Load mesh asset
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/Engine/BasicShapes/Sphere"));//CHATGPT
	if (MeshAsset.Succeeded())//CHATGPT
	{
		FoodMeshComponent->SetStaticMesh(MeshAsset.Object);//CHATGPT
	}
	else//CHATGPT
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to load mesh for AFood!"));//CHATGPT
	}
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	CreateFood();//CHATGPT
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::CreateFood()//CHATGPT
{
	// Set the spawn location (you can customize this based on your requirements)
	FVector SpawnLocation = FVector(0, 0, 2000);//CHATGPT

	// Spawn a new instance of AFood at the specified location
	GetWorld()->SpawnActor<AFood>(AFood::StaticClass(), SpawnLocation, FRotator::ZeroRotator);//CHATGPT
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
			CreateFood();
		}
	}
}

